<?php

/**
 * Overview page for Stripe user account.
 */
function stripe_managed_accounts_user_overview($user) {
  drupal_set_title(t('Stripe account'));
  $page = array();

  $stripe_account = stripe_managed_accounts_user_account_load($user->uid, TRUE);

  if ($stripe_account) {
    $page['content'] = stripe_managed_accounts_user_overview_content($stripe_account, $user);
  }
  else {
    drupal_set_title(t('Create a new Stripe account'));

    module_load_include('inc', 'stripe_managed_accounts', 'stripe_managed_accounts.forms');
    $page['stripe_add'] = drupal_get_form('stripe_managed_accounts_form', $stripe_account);
  }

  return $page;
}

/**
 * Overview page for Stripe user account.
 */
function stripe_managed_accounts_user_edit($user) {
  drupal_set_title(t('Edit your Stripe account'));
  $page = array();

  $stripe_account = stripe_managed_accounts_user_account_load($user->uid, TRUE);

  if ($stripe_account) {
    module_load_include('inc', 'stripe_managed_accounts', 'stripe_managed_accounts.forms');
    $page['stripe_add'] = drupal_get_form('stripe_managed_accounts_form', $stripe_account);
  }

  return $page;
}

/**
 * Content for overview page.
 */
function stripe_managed_accounts_user_overview_content($stripe_account, $account) {
  $content = array(
    '#theme' => 'user_profile',
    '#account' => $account,
    '#view_mode' => 'full',
  );

  $map_account_type = array(
    'individual' => t('Individual'),
    'company' => t('Company'),
  );
  $content['account_info'] = array(
    '#type' => 'user_profile_category',
    '#title' => t('Account summary'),
  );
  if ($stripe_account->legal_entity->first_name || $stripe_account->legal_entity->last_name) {
    $content['account_info']['full_name'] = array(
      '#theme' => 'user_profile_item',
      '#title' => t('Full name'),
      '#markup' => implode(' ', array(
        $stripe_account->legal_entity->first_name,
        $stripe_account->legal_entity->last_name,
      )),
    );
  }
  if ($stripe_account->legal_entity->business_name) {
    $content['account_info']['business_name'] = array(
      '#theme' => 'user_profile_item',
      '#title' => t('Business name'),
      '#markup' => $stripe_account->legal_entity->business_name,
    );
  }
  if ($stripe_account->legal_entity->type) {
    $content['account_info']['account_type'] = array(
      '#theme' => 'user_profile_item',
      '#title' => t('Account type'),
      '#markup' => $map_account_type[$stripe_account->legal_entity->type],
    );
  }

  $verification_status = $stripe_account->legal_entity->verification->status;
  $map_verification_status = array(
    'unverified' => t('Unverified'),
    'pending' => t('Pending'),
    'verified' => t('Verified'),
  );

  $content['verification_status'] = array(
    '#type' => 'user_profile_category',
    '#title' => t('Account verification'),
  );
  $content['verification_status']['status'] = array(
    '#status' => $stripe_account->legal_entity->verification->status,
    '#theme' => 'user_profile_item',
    '#title' => t('Verification status'),
    '#markup' => $map_verification_status[$verification_status],
    '#attributes' => array(
      'class' => array(
        'verification-status',
        "verification-status-$verification_status"
      ),
    ),
  );

  $content['development_details'] = array(
    '#type' => 'user_profile_category',
    '#title' => t('Development details'),
  );
  $content['development_details']['account_id'] = array(
    '#theme' => 'user_profile_item',
    '#title' => t('Account id'),
    '#markup' => $stripe_account->id,
  );
  $content['development_details']['key_secret'] = array(
    '#theme' => 'user_profile_item',
    '#title' => t('Secret key'),
    '#markup' => $stripe_account->key_secret,
  );
  $content['development_details']['key_publishable'] = array(
    '#theme' => 'user_profile_item',
    '#title' => t('Publishable key'),
    '#markup' => $stripe_account->key_publishable,
  );

  return $content;
}

/**
 * Overview page for Stripe bank accounts.
 */
function stripe_managed_accounts_user_bank_accounts($user) {
  drupal_set_title(t('Stripe bank accounts'));
  $page = array();

  $stripe_account = stripe_managed_accounts_user_account_load($user->uid);

  $page['content'] = array(
    '#theme' => 'user_profile',
    '#account' => $user,
    '#view_mode' => 'full',
  );

  $page['content']['bank_accounts'] = array(
    '#type' => 'user_profile_category',
    '#title' => t('Bank accounts'),
  );

  foreach ($stripe_account->external_accounts->data as $external_account) {
    if ($external_account->object == 'bank_account') {
      $page['content']['bank_accounts'][$external_account->id] = array(
        '#type' => 'fieldset',
        '#title' => $external_account->bank_name,
      );

      $page['content']['bank_accounts'][$external_account->id]['account_number'] = array(
        '#type' => 'user_profile_item',
        '#title' => t('Account number'),
        '#markup' => "****.****.****.{$external_account->last4}",
      );

      $page['content']['bank_accounts'][$external_account->id]['routing_number'] = array(
        '#type' => 'user_profile_item',
        '#title' => t('Routing number'),
        '#markup' => $external_account->routing_number,
      );

      module_load_include('inc', 'stripe_managed_accounts', 'stripe_managed_accounts.api');
      $countries = stripe_managed_accounts_api_schema_countries();
      $page['content']['bank_accounts'][$external_account->id]['country'] = array(
        '#type' => 'user_profile_item',
        '#title' => t('Country'),
        '#markup' => $countries[$external_account->country] . ' (' . strtoupper($external_account->currency) . ')',
      );

      $page['content']['bank_accounts'][$external_account->id]['delete_form'] = array(
        '#type' => 'form',
        '#action' => url("user/{$user->uid}/stripe/bank_accounts/{$external_account->id}/delete"),
      );

      $page['content']['bank_accounts'][$external_account->id]['delete_form']['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Remove bank account'),
      );
    }
  }

  if ($stripe_account->legal_entity->first_name || $stripe_account->legal_entity->last_name) {
    $content['account_info']['full_name'] = array(
      '#theme' => 'user_profile_item',
      '#title' => t('Full name'),
      '#markup' => implode(' ', array(
        $stripe_account->legal_entity->first_name,
        $stripe_account->legal_entity->last_name,
      )),
    );
  }

  return $page;
}

/**
 * Add a new Stripe bank account.
 */
function stripe_managed_accounts_user_bank_accounts_add($user) {
  drupal_set_title(t('Add a bank account'));
  $page = array();

  $stripe_account = stripe_managed_accounts_user_account_load($user->uid);
  module_load_include('inc', 'stripe_managed_accounts', 'stripe_managed_accounts.forms');
  $page['content']['form'] = drupal_get_form('stripe_managed_accounts_bank_account_form', $stripe_account);

  return $page;
}

/**
 * Delete a Stripe bank account.
 */
function stripe_managed_accounts_user_bank_accounts_delete($user, $bank_account_id) {
  $page = array();

  $stripe_account = stripe_managed_accounts_user_account_load($user->uid);

  foreach ($stripe_account->external_accounts->data as $external_account) {
    if ($external_account->id == $bank_account_id) {
      $page['content'] = drupal_get_form('stripe_managed_accounts_user_bank_accounts_delete_form', $stripe_account, $user, $external_account);
    }
  }


  return $page;
}
