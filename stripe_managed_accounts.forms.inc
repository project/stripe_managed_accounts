<?php

/**
 * Stripe Managed Account form.
 *
 * @param $form
 * @param $form_state
 * @param $stripe_account
 *   Provide a Stripe account object in order to make an edit form. Leave this
 *   parameter blank to make an add form.
 */
function stripe_managed_accounts_form($form, $form_state, $stripe_account = NULL) {
  $values = stripe_managed_accounts_form_values($stripe_account);

  $op = 'add';
  $form['stripe_account'] = array(
    '#type' => 'value',
    '#value' => NULL,
  );
  if ($stripe_account) {
    $op = 'edit';
    $form['stripe_account']['#value'] = $stripe_account;
  }

  if ($op == 'add') {
    $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('Account type'),
      '#options' => array(
        'individual' => t('Individual'),
        'company' => t('Company'),
      ),
      '#default_value' => 'individual',
    );
  }

  $form['legal_entity'] = array(
    '#title' => t('Personal details'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
  );
  $form['legal_entity']['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First name'),
    '#default_value' => $values['legal_entity']['first_name'],
  );
  $form['legal_entity']['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last name'),
    '#default_value' => $values['legal_entity']['last_name'],
  );
  $form['legal_entity']['dob'] = array(
    '#type' => 'date',
    '#title' => t('Date of birth'),
    '#default_value' => $values['legal_entity']['dob'],
  );

  if ($op == 'add') {
    $form['external_account']['#tree'] = TRUE;
    $form['external_account']['bank_account'] = array(
      '#title' => 'Bank Account details',
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );

    $bank_account_form = stripe_managed_accounts_bank_account_form(array(), array());

    $form['external_account']['bank_account'] = $form['external_account']['bank_account'] + $bank_account_form;
    unset($form['external_account']['bank_account']['actions']);

    $form['tos_acceptance'] = array(
      '#type' => 'checkbox',
      '#title' => t('By registering your account, you agree to <a href="@link">Stripe Connected Account Agreement</a>.', array(
        '@link' => 'https://stripe.com/connect/account-terms',
      )),
    );
  }

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Extract values from a given stripe account.
 *
 * @param $stripe_account
 *   The Stripe account object.
 *
 * @return
 *   An array with the extracted values. The $stripe_account param is optional.
 *   If none is given, the returned array will contain the data structure, but
 *   with empty values.
 */
function stripe_managed_accounts_form_values($stripe_account = NULL) {
  module_load_include('inc', 'stripe_managed_accounts', 'stripe_managed_accounts.api');
  $values = stripe_managed_accounts_api_schema_account();

  if ($stripe_account) {
    $values = _stripe_managed_accounts_form_values_fill($stripe_account, $stripe_account->keys(), $values);
  }

  return $values;
}

/**
 * Helper function to build the form values recursively.
 */
function _stripe_managed_accounts_form_values_fill($source, $keys, $values) {
  foreach ($keys as $key) {
    if (isset($values[$key])) {
      if (is_array($values[$key]) && is_object($source->$key)) {
        $values[$key] = _stripe_managed_accounts_form_values_fill($source->$key, $source->$key->keys(), $values[$key]);
      }
      else {
        $values[$key] = $source->$key;
      }
    }
  }

  return $values;
}

/**
 * Stripe Managed Account form.
 */
function stripe_managed_accounts_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $stripe_account = $values['stripe_account'];

  $op =
  $form_state['op'] = is_null($stripe_account) ? 'add' : 'edit';

  $params = array(
    'legal_entity' => array(
      'first_name' => $values['legal_entity']['first_name'],
      'last_name' => $values['legal_entity']['last_name'],
      'dob' => array(
        'year' => $values['legal_entity']['dob']['year'],
        'month' => $values['legal_entity']['dob']['month'],
        'day' => $values['legal_entity']['dob']['day'],
      )
    ),
  );

  switch ($op) {
    case 'add':
      $params['legal_entity']['type'] = $values['type'];
      $params['external_account'] = array(
        'object' => 'bank_account',
        'account_number' => $values['external_account']['bank_account']['account_number'],
        'country' => $values['external_account']['bank_account']['country'],
        'currency' => $values['external_account']['bank_account']['currency'],
        'routing_number' => $values['external_account']['bank_account']['routing_number'],
      );
      $params['tos_acceptance'] = array(
        'date' => REQUEST_TIME,
        'ip' => ip_address(),
        'user_agent' => $_SERVER['HTTP_USER_AGENT'],
      );

      if ($values['tos_acceptance']) {
        $stripe_account = stripe_managed_accounts_account_create($params);

        $form_state['stripe_account'] = $stripe_account;
      }
      else {
        drupal_set_message(t('You must agree with the Stripe Connected Account Agreement terms to create your account.'), 'error');
      }
      break;
    case 'edit':
      $stripe_account = stripe_managed_accounts_account_update($stripe_account->id, $params);

      $form_state['stripe_account'] = $stripe_account;
      break;
  }
}

/**
 * Stripe Managed Account bank account form.
 *
 * @param $form
 * @param $form_state
 * @param $stripe_account
 *   The Stripe account object.
 */
function stripe_managed_accounts_bank_account_form($form, $form_state, $stripe_account = NULL) {
  $form['stripe_account'] = array(
    '#type' => 'value',
    '#value' => NULL,
  );
  if ($stripe_account) {
    $form['stripe_account']['#value'] = $stripe_account;
  }

  $form['account_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Account number'),
  );
  $form['routing_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Rounting number'),
  );

  module_load_include('inc', 'stripe_managed_accounts', 'stripe_managed_accounts.api');
  $form['country'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#options' => stripe_managed_accounts_api_schema_countries(),
  );

  $form['currency'] = array(
    '#type' => 'select',
    '#title' => t('Currency'),
    '#options' => stripe_managed_accounts_api_schema_external_account_currencies(),
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add a new bank account'),
  );

  return $form;
}

/**
 * Stripe Managed Account bank account form.
 *
 * @param $form
 * @param $form_state
 * @param $stripe_account
 *   The Stripe account object.
 */
function stripe_managed_accounts_bank_account_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $stripe_account = $values['stripe_account'];
  $external_accounts = $stripe_account->external_accounts;

  $stripe_account->external_accounts->create(array(
    'external_account' => array(
      'object' => 'bank_account',
      'account_number' => $values['account_number'],
      'routing_number' => $values['routing_number'],
      'country' => $values['country'],
      'currency' => $values['currency'],
    ),
  ));

  $params['external_accounts'] = $stripe_account->external_accounts;

  stripe_managed_accounts_account_update($stripe_account->id, $params);
}
